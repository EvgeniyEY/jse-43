package ru.ermolaev.tm.exception.empty;

import ru.ermolaev.tm.exception.AbstractException;

public final class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty.");
    }

}
