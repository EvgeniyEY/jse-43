<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>TASK MANAGER</title>
    </head>
    <style><%@include file="/WEB-INF/views/resources/styles.css"%></style>

<body>
<table border="1" width="100%" height="100%" >

    <tr>
        <td width="50%" height="50px" class="font" style="padding-left: 7px"><a href="/" class="font">TASK-MANAGER</a></td>
        <td width="25%" align="center"><a href="/projects/show" class="font">PROJECTS</a></td>
        <td width="25%" align="center"><a href="/tasks/show" class="font">TASKS</a></td>
    </tr>

    <tr>
        <td colspan="3" height="100%" valign="top" style="padding: 10px">

