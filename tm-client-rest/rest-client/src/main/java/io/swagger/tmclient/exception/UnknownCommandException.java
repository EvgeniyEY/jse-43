package io.swagger.tmclient.exception;

import org.jetbrains.annotations.NotNull;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! This command does not exist.");
    }

    public UnknownCommandException(@NotNull final String command) {
        super("Error! This command [" + command + "] does not exist.");
    }

}
